﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp7
{
    public class ExtendedUser: User
    {
        public string LastName { get; set; }

        public int Age { get; set; }
    }
}
