﻿using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    //public interface IContext<out TEntity> where TEntity: MasterContext
    //{
    //    ITest Context { get; }
    //}

    //public interface ITest
    //{
    //    int Count { get; }
    //}

    //public class MasterContext: IContext<MasterContext>, ITest
    //{
    //    public int Count { get; }

    //    public ITest Context => this;

    //    public DbSet<ApplicationUser> Users { get; set; }

    //    public MasterContext Context => this;
    //}

    //public class ExtendedContext: MasterContext, IContext<ExtendedContext>
    //{
    //    public DbSet<ApplicationRole> Roles { get; set; }

    //    public new ExtendedContext Context => this;
    //}

    //public class BAgreementManager
    //{
    //    private readonly IContext<MasterContext> _context;

    //    public BAgreementManager(IContext<MasterContext> context)
    //    {
    //        _context = context;
    //    }
    //}

    //public class ExtendedBAgreementManager: BAgreementManager
    //{
    //    public ExtendedBAgreementManager(
    //        IContext<ExtendedContext> context): base(context)
    //    {

    //    }
    //}

    class Program
    {
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            var a = "0,6";
            var b = Convert.ToDecimal(a, CultureInfo.InvariantCulture);

            Console.ReadLine();
        }
    }
}
