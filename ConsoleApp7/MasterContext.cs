﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp7
{
    public class MasterContext<TContext>: DbContext where TContext:DbContext
    {
        public MasterContext(): base()
        {
        }

        public MasterContext(DbContextOptions<TContext> options)
            : base(options)
        {

        }

        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var connectionString = "Data Source =.\\SQLEXPRESS; Initial Catalog = TestEF; Integrated Security = true;";
                optionsBuilder.UseSqlServer(connectionString);
            }
        }
    }
}
