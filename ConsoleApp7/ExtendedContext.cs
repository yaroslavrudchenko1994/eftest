﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp7
{
    public class ExtendedContext: MasterContext<ExtendedContext>
    {
        public ExtendedContext(DbContextOptions<ExtendedContext> options)
            : base(options)
        {

        }

        public new DbSet<ExtendedUser> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var connectionString = "Data Source =.\\SQLEXPRESS; Initial Catalog = TestEF; Integrated Security = true;";
                optionsBuilder.UseSqlServer(connectionString);
            }
        }
    }
}
